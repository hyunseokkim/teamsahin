// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTaskNode_EnemyAttack.h"
#include <BehaviorTree/BlackboardComponent.h>

UBTTaskNode_EnemyAttack::UBTTaskNode_EnemyAttack()
{
	bNotifyTick = true;
}

EBTNodeResult::Type UBTTaskNode_EnemyAttack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	return EBTNodeResult::InProgress;
}

void UBTTaskNode_EnemyAttack::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);

	//UE_LOG(LogTemp, Warning, TEXT("UBTTaskNode_EnemyAttack"));
	UBlackboardComponent* blackboardComp = OwnerComp.GetBlackboardComponent();
	if (blackboardComp == nullptr)
	{
		return;
	}
	if (!blackboardComp->GetValueAsBool(mCanAttack.SelectedKeyName))
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
		return;
	}
	//FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
}
