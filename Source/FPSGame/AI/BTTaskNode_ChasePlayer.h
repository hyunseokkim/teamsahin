// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTaskNode_ChasePlayer.generated.h"

/**
 * 
 */
UCLASS()
class FPSGAME_API UBTTaskNode_ChasePlayer : public UBTTaskNode
{
	GENERATED_BODY()

	UBTTaskNode_ChasePlayer(const FObjectInitializer& objectInitializer = FObjectInitializer::Get());
public:
private:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual FString GetStaticDescription() const override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
public:
private:
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector mPlayerKey;
	
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector mCanSeePlayer;
};
