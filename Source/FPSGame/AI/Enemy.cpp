// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "FPSProjectile.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	mWalkSpeed = 200;
	mRunSpeed = 400;
	
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	mAIController = Cast<AChasingAIController>(GetController());
	mCharacterMovement = Cast<UCharacterMovementComponent>(GetCharacterMovement());
	mCharacterMovement->MaxWalkSpeed = mWalkSpeed;
	
	//mCapsuleComponent = Cast<UCapsuleComponent>(GetCapsuleComponent());
	//mCapsuleComponent;
	mbDeath = false;
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	mbCanAttack = mAIController->mbCanAttack;
	//UE_LOG(LogTemp, Warning, TEXT("Can See Player: %d"), mAIController->mbCanSeePlayer);

	if (mbDamaged == false && mbDeath == false)
	{
		if (mAIController->mbCanSeePlayer)
		{
			mCharacterMovement->MaxWalkSpeed = mRunSpeed;
		}
		else
		{
			mCharacterMovement->MaxWalkSpeed = mWalkSpeed;
			//mAIController->ResetSight();
		}
	}
	
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::NotifyActorBeginOverlap(AActor* OtherActor)
{
	if (Cast<AFPSProjectile>(OtherActor))// && !OtherActor->GetName().Contains(FString("Enemy")))
	{
		
	}
	else
	{
		
	}
	
}

void AEnemy::NotifyActorEndOverlap(AActor* OtherActor)
{
	
}

