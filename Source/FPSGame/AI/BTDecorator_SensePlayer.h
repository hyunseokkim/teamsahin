// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDecorator_SensePlayer.generated.h"

/**
 * 
 */
UCLASS()
class FPSGAME_API UBTDecorator_SensePlayer : public UBTDecorator
{
	GENERATED_BODY()
	UBTDecorator_SensePlayer(const FObjectInitializer& objectInitializer = FObjectInitializer::Get());
public:
protected:
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void OnCeaseRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
private:
	virtual EBlackboardNotificationResult OnBlackboardKeyValueChange(const UBlackboardComponent& Blackboard, FBlackboard::FKey ChangedKeyID);
public:
protected:
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector mCanSeePlayerKey;
private:
};
