// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTService_UpdateChasing.generated.h"

/**
 * 
 */
UCLASS()
class FPSGAME_API UBTService_UpdateChasing : public UBTService
{
	GENERATED_BODY()
	
	UBTService_UpdateChasing(const FObjectInitializer& objectInitializer = FObjectInitializer::Get());
public:
protected:
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	virtual FString GetStaticServiceDescription() const override;
private:
public:
private:
	UPROPERTY(EditAnywhere, Category = PlayerClass)
	TSubclassOf<AActor> mPlayerClass;
	
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector mCanSeePlayerKey;

	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector mPlayerKey;

	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector mLastKnownPositionKey;

	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector mAttackRangeKey;

	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector mCanAttack;
	
	bool mbLastCanSeePlayer;

	AActor* mPlayer = nullptr;
};
