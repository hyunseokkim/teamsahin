// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAnimInstance.h"
#include "Enemy.h"

void UEnemyAnimInstance::NativeInitializeAnimation()
{
	if (mPawn == nullptr)
	{
		mPawn = TryGetPawnOwner();
		if (mPawn)
		{
			mEnemy = Cast<AEnemy>(mPawn);
		}
	}
}

void UEnemyAnimInstance::UpdateAnimationProperties()
{
	if (mPawn == nullptr)
	{
		mPawn = TryGetPawnOwner();
		if (mPawn)
		{
			mEnemy = Cast<AEnemy>(mPawn);
		}
	}
	if (mPawn)
	{
		FVector speed = mPawn->GetVelocity();
		FVector lateralSpeed = FVector(speed.X, speed.Y, 0.f);
		mMovementSpeed = lateralSpeed.Size();
	}
}
