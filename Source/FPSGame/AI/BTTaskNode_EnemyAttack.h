// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTaskNode_EnemyAttack.generated.h"

/**
 * 
 */
UCLASS()
class FPSGAME_API UBTTaskNode_EnemyAttack : public UBTTaskNode
{
	GENERATED_BODY()
public:
	UBTTaskNode_EnemyAttack();
protected:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
private:
public:
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector mCanAttack;
private:
};
