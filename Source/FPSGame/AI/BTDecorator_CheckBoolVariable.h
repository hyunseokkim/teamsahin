// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDecorator_CheckBoolVariable.generated.h"

/**
 * 
 */
UCLASS()
class FPSGAME_API UBTDecorator_CheckBoolVariable : public UBTDecorator
{
	GENERATED_BODY()
	
	UBTDecorator_CheckBoolVariable(const FObjectInitializer& objectInitializer = FObjectInitializer::Get());
public:
protected:
private:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
	virtual EBlackboardNotificationResult OnBlackboardKeyValueChange(const UBlackboardComponent& Blackboard, FBlackboard::FKey ChangedKeyID);
	virtual FString GetStaticDescription() const override;
public:
protected:
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector BoolVariableToCheck;
private:
};
