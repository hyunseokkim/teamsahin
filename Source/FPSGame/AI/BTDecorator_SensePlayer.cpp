// Fill out your copyright notice in the Description page of Project Settings.


#include "BTDecorator_SensePlayer.h"
#include <BehaviorTree/BlackboardComponent.h>

UBTDecorator_SensePlayer::UBTDecorator_SensePlayer(const FObjectInitializer& objectInitializer) :
	Super(objectInitializer)
{
	NodeName = "Sense Player";
	
	bNotifyBecomeRelevant = true;
	bNotifyCeaseRelevant = true;
}

void UBTDecorator_SensePlayer::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	// Get Black board component
	UBlackboardComponent* blackboardComp = OwnerComp.GetBlackboardComponent();
	if (blackboardComp == nullptr)
	{
		return;
	}

	// Register observer
	blackboardComp->RegisterObserver(mCanSeePlayerKey.GetSelectedKeyID(), this,
		FOnBlackboardChangeNotification::CreateUObject(this, &UBTDecorator_SensePlayer::OnBlackboardKeyValueChange));
}

void UBTDecorator_SensePlayer::OnCeaseRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	// Get Black board component
	UBlackboardComponent* blackboardComp = OwnerComp.GetBlackboardComponent();
	if (blackboardComp == nullptr)
	{
		return;
	}

	// Unregister observer
	blackboardComp->UnregisterObserversFrom(this);
}

EBlackboardNotificationResult UBTDecorator_SensePlayer::OnBlackboardKeyValueChange(
	const UBlackboardComponent& Blackboard, FBlackboard::FKey ChangedKeyID)
{
	UBehaviorTreeComponent* BehaviorComp = (UBehaviorTreeComponent*)Blackboard.GetBrainComponent();
	if (BehaviorComp == nullptr)
	{
		return EBlackboardNotificationResult::RemoveObserver;
	}

	if (mCanSeePlayerKey.GetSelectedKeyID() == ChangedKeyID)
	{
		BehaviorComp->RequestExecution(this);
	}
	return EBlackboardNotificationResult::ContinueObserving;
}
