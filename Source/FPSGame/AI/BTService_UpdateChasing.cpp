// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_UpdateChasing.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "ChasingAIController.h"


UBTService_UpdateChasing::UBTService_UpdateChasing(const FObjectInitializer& objectInitializer) :
	Super(objectInitializer)
{
	NodeName = "Update Chasing Behavior";
	
	bNotifyBecomeRelevant = true;
	bNotifyCeaseRelevant = false;
	mbLastCanSeePlayer = false;

	// Filter the Blackboard Key Selectors
	/*mPlayerKey.AddObjectFilter(this,
		GET_MEMBER_NAME_CHECKED(UBTService_UpdateChasing, mPlayerKey), AActor::StaticClass());
	mLastKnownPositionKey.AddVectorFilter(this,
		GET_MEMBER_NAME_CHECKED(UBTService_UpdateChasing, mLastKnownPositionKey));
	mCanSeePlayerKey.AddBoolFilter(this,
		GET_MEMBER_NAME_CHECKED(UBTService_UpdateChasing, mCanSeePlayerKey));*/
}

void UBTService_UpdateChasing::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UE_LOG(LogTemp, Warning, TEXT("UBTService_UpdateChasing:: OnBecomeRelevant"));
	// Get Blackboard Component
	UBlackboardComponent* blackboardComp = OwnerComp.GetBlackboardComponent();
	if (blackboardComp == nullptr)
	{
		return;
	}

	if (!mPlayerKey.IsSet())
	{
		// Retrieve Player and Update the Blackboard
		TArray<AActor*> foundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), mPlayerClass, foundActors);
		if (foundActors[0])
		{
			blackboardComp->SetValueAsObject(mPlayerKey.SelectedKeyName, foundActors[0]);
			mPlayer = foundActors[0];
		}
	}

	// Get AI Controller
	AAIController* aiController = OwnerComp.GetAIOwner();
	if (aiController == nullptr)
	{
		return;
	}

	// Get ChasingAiController
	AChasingAIController* chasingController = Cast<AChasingAIController>(aiController);
	if (chasingController == nullptr)
	{
		return;
	}
	blackboardComp->SetValueAsFloat(mAttackRangeKey.SelectedKeyName, chasingController->mAttackRange);
}

void UBTService_UpdateChasing::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	// Get Blackboard Component
	UBlackboardComponent* blackboardComp = OwnerComp.GetBlackboardComponent();
	if (blackboardComp == nullptr)
	{
		return;
	}

	// Get AI Controller
	AAIController* aiController = OwnerComp.GetAIOwner();
	if (aiController == nullptr)
	{
		return;
	}

	// Get ChasingAiController
	AChasingAIController* chasingController = Cast<AChasingAIController>(aiController);
	if (chasingController == nullptr)
	{
		return;
	}

	// Update the Blackboard with the current value of CanSeePlayer from the Chasing Controoler
	blackboardComp->SetValueAsBool(mCanSeePlayerKey.SelectedKeyName, chasingController->mbCanSeePlayer);

	// If the LastCanSeePlayer is different from the current one, then update the LastKnownPlayerPosition
	if (chasingController->mbCanSeePlayer)// != mbLastCanSeePlayer)
	{
		blackboardComp->SetValueAsVector(mLastKnownPositionKey.SelectedKeyName, 
											chasingController->mLastKnownPlayerPosition);
		
		// update player distance
		const float distance = mPlayer->GetDistanceTo(aiController->GetPawn());// chasingController->GetOwner()->GetDistanceTo(mPlayer);
		
		//UE_LOG(LogTemp, Warning, TEXT("distance: %f\n"), 
			//distance);// (aiController->GetPawn() == nullptr ? TEXT("Fail") : TEXT("Success")));
		/*auto player = blackboardComp->GetValueAsObject(mPlayerKey.SelectedKeyName);
		if (player)
		{
			distance = FVector::Distance(chasingController->GetOwner()->GetActorLocation(),
				Cast<AActor>(blackboardComp->GetValueAsObject(mPlayerKey.SelectedKeyName))->GetActorLocation());
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Fail to Get Object"));
		}*/
		const bool canAttack = (distance <= chasingController->mAttackRange);
		chasingController->mbCanAttack = canAttack;
		blackboardComp->SetValueAsBool(mCanAttack.SelectedKeyName, canAttack);
		//UE_LOG(LogTemp, Warning, TEXT("CanAttack: %d\tAttackRange: %f\n"), canAttack, chasingController->mAttackRange);
	}

	// Update the LastCanSeePlayer with the current CanSeePlayer
	mbLastCanSeePlayer = chasingController->mbCanSeePlayer;

	// Call to the parent TickNode
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}

FString UBTService_UpdateChasing::GetStaticServiceDescription() const
{
	FString description;// = Super::GetStaticDescription();
	description += "\n\n";
	description += FString::Printf(TEXT("%s: '%s'"), TEXT("Player Class"),
	                                      mPlayerClass ? *mPlayerClass->GetName() : TEXT(""));
	description.Append(FString::Printf(TEXT("\n%s: '%s'"), TEXT("PlayerKey"), mPlayerKey.IsSet() ? *mPlayerKey.SelectedKeyName.ToString() : TEXT("")));
	description.Append(FString::Printf(TEXT("\n%s: '%s'"), TEXT("CanSeePlayerKey"), 
		mCanSeePlayerKey.IsSet() ? *mCanSeePlayerKey.SelectedKeyName.ToString() : TEXT("")));
	return description;
}
