// Fill out your copyright notice in the Description page of Project Settings.


#include "BTDecorator_CheckBoolVariable.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTDecorator_CheckBoolVariable::UBTDecorator_CheckBoolVariable(const FObjectInitializer& objectInitializer):
	Super(objectInitializer)
{
	NodeName = "Check Bool Variable";
	
	/*BoolVariableToCheck.AddBoolFilter(this, 
		GET_MEMBER_NAME_CHECKED(UBTDecorator_CheckBoolVariable, BoolVariableToCheck));*/
}

bool UBTDecorator_CheckBoolVariable::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp,
	uint8* NodeMemory) const
{
	// Get Blackboard Component
	const UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
	if (BlackboardComp == nullptr)
	{
		return false;
	}

	// Perform Boolean Variable Check
	return BlackboardComp->GetValueAsBool(BoolVariableToCheck.SelectedKeyName);
}

EBlackboardNotificationResult UBTDecorator_CheckBoolVariable::OnBlackboardKeyValueChange(
	const UBlackboardComponent& Blackboard, FBlackboard::FKey ChangedKeyID)
{
	return EBlackboardNotificationResult::ContinueObserving;
}

FString UBTDecorator_CheckBoolVariable::GetStaticDescription() const
{
	FString returnDesc = Super::GetStaticDescription();
	returnDesc += "\n\n";
	returnDesc += FString::Printf(TEXT("%s: '%s'"), TEXT("Bool Variable to Check"),
		BoolVariableToCheck.IsSet() ? *BoolVariableToCheck.SelectedKeyName.ToString() : TEXT(""));
	return returnDesc;
}
