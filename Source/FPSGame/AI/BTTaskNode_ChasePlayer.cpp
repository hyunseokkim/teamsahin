// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTaskNode_ChasePlayer.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTTaskNode_ChasePlayer::UBTTaskNode_ChasePlayer(const FObjectInitializer& objectInitializer)
{
	NodeName = "Chase Player";
	bNotifyTick = true;
}

EBTNodeResult::Type UBTTaskNode_ChasePlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UE_LOG(LogTemp, Warning, TEXT("Excute Chase Player"));
	// Get Blackboard Component
	UBlackboardComponent* blackboardComp = OwnerComp.GetBlackboardComponent();
	if (blackboardComp == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	bool bCanSeePlayer = false;
	/*while(blackboardComp->GetValueAsBool(mCanSeePlayer.SelectedKeyName))
	{
		;
	}*/

	return EBTNodeResult::InProgress;
}

FString UBTTaskNode_ChasePlayer::GetStaticDescription() const
{
	FString description;

	return description;
}

void UBTTaskNode_ChasePlayer::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	UE_LOG(LogTemp, Warning, TEXT("Tick Chase Player"));
	// Get Blackboard Component
	UBlackboardComponent* blackboardComp = OwnerComp.GetBlackboardComponent();
	if (blackboardComp == nullptr)
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
		return;
	}

	if (!blackboardComp->GetValueAsBool(mCanSeePlayer.SelectedKeyName))
	{
		UE_LOG(LogTemp, Warning, TEXT("[Finished] Tick Chase Player"));
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}
