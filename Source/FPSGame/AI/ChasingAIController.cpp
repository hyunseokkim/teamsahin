// Fill out your copyright notice in the Description page of Project Settings.


#include "ChasingAIController.h"
#include <FPSCharacter.h>
#include <BehaviorTree/BehaviorTree.h>
#include "FPSProjectile.h"

AChasingAIController::AChasingAIController(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
	// Creating the AI Perception Component
	//PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("SightPerceptionComponet"));
	PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionComponent"));
	// Create the Sight Sense and Configure it
	mSightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(FName("Sight Config"));
	mSightConfig->DetectionByAffiliation.bDetectEnemies = true;
	mSightConfig->DetectionByAffiliation.bDetectNeutrals = true;
	mSightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	mSightConfig->SightRadius = 1500;
	mSightConfig->LoseSightRadius = 2000;

	// Create the Hearing Sense and Configure it
	mHearingConfig = CreateDefaultSubobject<UAISenseConfig_Hearing>(FName("Hearing Config"));
	mHearingConfig->DetectionByAffiliation.bDetectEnemies = true;
	mHearingConfig->DetectionByAffiliation.bDetectNeutrals = true;
	mHearingConfig->DetectionByAffiliation.bDetectFriendlies = true;
	mHearingConfig->HearingRange = 4000;

	// Assigning the Sight and Hearing sense to the AI Perception Component
	PerceptionComponent->ConfigureSense(*mSightConfig);
	PerceptionComponent->ConfigureSense(*mHearingConfig);
	PerceptionComponent->SetDominantSense(mSightConfig->GetSenseImplementation());


	// Binding the onTargetPerceptionUpdate function
	PerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this,
		&AChasingAIController::onTargetPerceptionUpdate);

	// Assign to Team 1
	SetGenericTeamId(FGenericTeamId(1));

	mbCanAttack = false;
}

void AChasingAIController::onTargetPerceptionUpdate(AActor* actor, FAIStimulus stimulus)
{

	UE_LOG(LogTemp, Warning, TEXT("[onTargetPerceptionUpdate: %d]"), stimulus.WasSuccessfullySensed());
	if (Cast<AFPSCharacter>(actor) != nullptr)// && actor->GetActorLabel().Contains("Player"))
	{

		if (stimulus.Type == mHearingConfig->GetSenseID()) // Hearing
		{
			const float distance = actor->GetDistanceTo(this->GetPawn());
			mSightConfig->SightRadius = distance + 100;
			mSightConfig->LoseSightRadius = mSightConfig->SightRadius + 300;
			UE_LOG(LogTemp, Warning, TEXT("@@@@@@@@@@@[SightRadius: %f, LoseRadius: %f]"),
				mSightConfig->SightRadius, mSightConfig->LoseSightRadius);
		}
		else // Sight
		{
			mSightConfig->SightRadius = mOriginSightRadius;
			mSightConfig->LoseSightRadius = mOriginLoseSightRadius;
		}
		mLastKnownPlayerPosition = stimulus.StimulusLocation;
		mbCanSeePlayer = stimulus.WasSuccessfullySensed();
		UE_LOG(LogTemp, Warning, TEXT("@@@@@@@@@@@[AChasingAIController] mbCanSeePlayer: %d"), mbCanSeePlayer);

	}
	else
	{

		//UE_LOG(LogTemp, Warning, TEXT("Faile To cast Player\n"));
	}
}


void AChasingAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if (mBehaviorTree != nullptr)
	{
		RunBehaviorTree(mBehaviorTree);
	}
}

