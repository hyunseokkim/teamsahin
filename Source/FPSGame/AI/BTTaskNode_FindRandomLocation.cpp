// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTaskNode_FindRandomLocation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "NavigationSystem.h"
#include "AIController.h"

UBTTaskNode_FindRandomLocation::UBTTaskNode_FindRandomLocation(const FObjectInitializer& objectInitializer):
	Super(objectInitializer)
{
	NodeName = "Find Random Location";

	/*DestinationVector.AddVectorFilter(this,
		GET_MEMBER_NAME_CHECKED(UBTTaskNode_FindRandomLocation, DestinationVector));*/
}

EBTNodeResult::Type UBTTaskNode_FindRandomLocation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UE_LOG(LogTemp, Warning, TEXT("Excute Find Random Location"));
	// Get Blackboard Component
	UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
	if (BlackboardComp == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	// Get Controlled Pawn
	APawn* ControlledPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (!ControlledPawn)
	{
		return EBTNodeResult::Failed;
	}

	// Get Navigation System
	UNavigationSystemV1* NavSys = UNavigationSystemV1::GetCurrent(GetWorld());
	if (NavSys == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	// Prepare variables for Query
	FNavLocation Result;
	const FVector Origin = ControlledPawn->GetActorLocation();

	// Perform Query
	const bool bSuccess = NavSys->GetRandomReachablePointInRadius(Origin, Radius, Result);
	if (!bSuccess)
	{
		return EBTNodeResult::Failed;
	}

	// Save Result and return success
	BlackboardComp->SetValueAsVector(DestinationVector.SelectedKeyName, Result.Location);
	
	return EBTNodeResult::Succeeded;
}

FString UBTTaskNode_FindRandomLocation::GetStaticDescription() const
{
	FString description;// = Super::GetStaticDescription();
	description += "\n\n";
	description += FString::Printf(TEXT("%s: '%s'"), TEXT("DestinationKey"),
		DestinationVector.IsSet() ? *DestinationVector.SelectedKeyName.ToString() : TEXT(""));
	description += FString::Printf(TEXT("\n%s: '%s'"), TEXT("Radius"), *FString::SanitizeFloat(Radius));
	return description;
}

void UBTTaskNode_FindRandomLocation::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	UE_LOG(LogTemp, Warning, TEXT("Tick Find Random Location"));
}
