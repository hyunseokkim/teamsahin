// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ChasingAIController.h"
#include "Enemy.generated.h"


UENUM(BlueprintType)
enum class EEnemyType : uint8
{
	Wolf UMETA(DisplayName = "Wolf"),
	WoodZombie UMETA(DisplayName = "WoodZombie")

};

class UCharacterMovementComponent;
UCLASS()
class FPSGAME_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void NotifyActorEndOverlap(AActor* OtherActor)override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enemy)
	float mWalkSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enemy)
	float mRunSpeed;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Enemy)
	bool mbCanAttack = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enemy)
	bool mbDamaged;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enemy)
	bool mbDeath;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enemy)
	int mHealth = 2;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enemy)
	bool mbIsHitting = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enemy)
	EEnemyType mEnemyType;
private:
	AChasingAIController* mAIController;
	UCharacterMovementComponent*	mCharacterMovement;
	//UCapsuleComponent* mCapsuleComponent;
};
