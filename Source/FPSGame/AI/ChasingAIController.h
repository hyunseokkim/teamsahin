// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include <Perception/AIPerceptionComponent.h>
#include <Perception/AISense_Sight.h>
#include <Perception/AISenseConfig_Sight.h>
#include <Perception/AISense_Hearing.h>
#include <Perception/AISenseConfig_Hearing.h>
#include "ChasingAIController.generated.h"

/**
 *
 */
UCLASS()
class FPSGAME_API AChasingAIController : public AAIController
{
	GENERATED_BODY()
		AChasingAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
public:
	//virtual void Tick(float DeltaSeconds) override;
protected:
	UFUNCTION()
		void onTargetPerceptionUpdate(AActor* actor, FAIStimulus stimulus);

private:
	virtual void OnPossess(APawn* InPawn) override;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		FVector mLastKnownPlayerPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		bool mbCanSeePlayer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float mAttackRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		bool mbCanAttack;

	UPROPERTY(EditAnywhere)
		UBehaviorTree* mBehaviorTree;
private:
	UAISenseConfig_Sight* mSightConfig;
	UAISenseConfig_Hearing* mHearingConfig;

	float mOriginSightRadius;
	float mOriginLoseSightRadius;
};
