/* Start Header -------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: <BlackHole.cpp>
Purpose: <player ability to create black hole>
Language: <c++>
Platform: <Unreal Engine>
Project: <GAM300>
Author: <kevin kim: hyunseok.kim>
End Header --------------------------------------------------------*/ 

#include "BlackHole.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "FPSProjectile.h"
#include "FPSCharacter.h"

// Sets default values
ABlackHole::ABlackHole()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create Mesh Component
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	//Disable Collision
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = MeshComp;

	InnerSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("InnerSphereComp"));
	InnerSphereComponent->SetSphereRadius(100);
	InnerSphereComponent->SetupAttachment(MeshComp);

	OuterSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("OuterSphereComp"));
	OuterSphereComponent->SetSphereRadius(3000);
	OuterSphereComponent->SetupAttachment(MeshComp);

	OuterRadialForce = -4000.f;
	InnerRadialForce = 4000.f;
}

// Called every frame
void ABlackHole::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Find all overlapping components that can collide and may be physically simulating.
	TArray<UPrimitiveComponent*> OverlappingComps;
	OuterSphereComponent->GetOverlappingComponents(OverlappingComps);
	float OuterSphereRadius = OuterSphereComponent->GetScaledSphereRadius();
	float InnerSphereRadius = InnerSphereComponent->GetScaledSphereRadius();


	for (int32 i = 0; i < OverlappingComps.Num(); i++)
	{
		UPrimitiveComponent* PrimComp = OverlappingComps[i];
		if (PrimComp)
		{
			// the component we are looking for! It needs to be simulating in order to apply forces.

			if (PrimComp->IsGravityEnabled())
			{
				PrimComp->AddRadialForce(GetActorLocation(), OuterSphereRadius, OuterRadialForce * 0.2f, ERadialImpulseFalloff::RIF_Constant, true);
			}
			else
			{
				if (Cast<AFPSProjectile>(PrimComp->GetOwner()))
				{
					PrimComp->AddRadialForce(GetActorLocation(), OuterSphereRadius, OuterRadialForce * 2.f, ERadialImpulseFalloff::RIF_Constant, true);
					PrimComp->AddRadialForce(GetActorLocation(), InnerSphereRadius, InnerRadialForce * 2.f, ERadialImpulseFalloff::RIF_Constant, true);
				}
				else
				{
					PrimComp->AddRadialForce(GetActorLocation(), OuterSphereRadius, OuterRadialForce * 1.f, ERadialImpulseFalloff::RIF_Constant, true);
				}
				PrimComp->SetPhysicsLinearVelocity(this->GetVelocity() * 0.03f, true);
			}
		}
	}
	
}

