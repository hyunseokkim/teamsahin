/* Start Header -------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: <SWeapon.cpp>
Purpose: <base case for weapon>
Language: <c++>
Platform: <Unreal Engine>
Project: <GAM300>
Author: <kevin kim: hyunseok.kim>
End Header --------------------------------------------------------*/ 
#include "SWeapon.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "CollisionQueryParams.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Camera/CameraShake.h"

// Sets default values
ASWeapon::ASWeapon()
{
	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;

	MuzzleSocketName = "Muzzle";
	TracerTargetName = "BeamEnd";
}

void ASWeapon::Fire()
{
	//Trace the world, from camera to crosshair location

	AActor* MyOwner = GetOwner();
	if(MyOwner)
	{
		//FVector EyeLocation = MyOwner->GetFirstPersonCameraComponent()->;
		FVector EyeLocation;
		FRotator EyeRotation;
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		const auto end = EyeLocation + EyeRotation.Vector() * 50000;

		FVector ShotDirection = EyeRotation.Vector();
		//FVector TraceEnd = EyeLocation + (ShotDirection * 10000);
		//FVector TraceEnd = EyeLocation + (ShotDirection * 50000);
		FVector TraceEnd = EyeLocation + (EyeRotation.Vector() * 50000);
		
		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(MyOwner);
		QueryParams.AddIgnoredActor(this);//ignore weapon
		QueryParams.bTraceComplex = true;

		//Particle target parameter
		FVector TracerEndPoint = TraceEnd;
		
		HitLocation = TraceEnd;

		FHitResult Hit;
		if(GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, ECC_Pawn, QueryParams))
		{
			//Blocking hit! process damage
			AActor* HitActor = Hit.GetActor();
			UGameplayStatics::ApplyPointDamage(HitActor, 20.0f, ShotDirection, Hit, MyOwner->GetInstigatorController(), this, DamageType);

			auto spawnLocationToTarget = Hit.Location - SpawnLocation;
			spawnLocationToTarget.Normalize();
			
			if(ImpactEffect)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
			}
			
			TracerEndPoint = Hit.ImpactPoint;
			HitLocation = Hit.Location;
		}
		else
		{
			HitLocation = TraceEnd;
		}

	}
}

void ASWeapon::FireWithEffect()
{
	//Trace the world, from camera to crosshair location

	AActor* MyOwner = GetOwner();
	if (MyOwner)
	{
		//FVector EyeLocation = MyOwner->GetFirstPersonCameraComponent()->;
		FVector EyeLocation;
		FRotator EyeRotation;
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		const auto end = EyeLocation + EyeRotation.Vector() * 50000;

		FVector ShotDirection = EyeRotation.Vector();
		//FVector TraceEnd = EyeLocation + (ShotDirection * 10000);
		//FVector TraceEnd = EyeLocation + (ShotDirection * 50000);
		FVector TraceEnd = EyeLocation + (EyeRotation.Vector() * 50000);

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(MyOwner);
		QueryParams.AddIgnoredActor(this);//ignore weapon
		QueryParams.bTraceComplex = true;

		//Particle target parameter
		FVector TracerEndPoint = TraceEnd;

		HitLocation = TraceEnd;

		FHitResult Hit;
		if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, ECC_Pawn, QueryParams))
		{
			//Blocking hit! process damage
			AActor* HitActor = Hit.GetActor();
			UGameplayStatics::ApplyPointDamage(HitActor, 20.0f, ShotDirection, Hit, MyOwner->GetInstigatorController(), this, DamageType);

			auto spawnLocationToTarget = Hit.Location - SpawnLocation;
			spawnLocationToTarget.Normalize();

			if (ImpactEffect)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
			}

			TracerEndPoint = Hit.ImpactPoint;
			HitLocation = Hit.Location;
		}
		else
		{
			HitLocation = TraceEnd;
		}
		//DrawDebugLine(GetWorld(), EyeLocation, TraceEnd, FColor::Magenta, false, 1.f, 0, 1.f);


		PlayFireEffects(TracerEndPoint);
	}
}

void ASWeapon::PlayFireEffects(FVector TracerEndPoint)
{
	if (MuzzleEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);
	}
	if (MuzzleEffect)
	{
		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
		auto Directional_vector = TracerEndPoint - MuzzleLocation;
		Directional_vector.Normalize();
		MuzzleLocation += Directional_vector * 200.f;
		
		UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, MuzzleLocation);
		if (TracerComp)
		{
			TracerComp->SetVectorParameter(TracerTargetName, TracerEndPoint);
		}
	}

	APawn* myOwner = Cast<APawn>(GetOwner());
	if(myOwner)
	{
		APlayerController* PC = Cast<APlayerController>(myOwner->GetController());
		if(PC)
		{
			PC->ClientPlayCameraShake(FireCamShake);
		}
	}
	
}
