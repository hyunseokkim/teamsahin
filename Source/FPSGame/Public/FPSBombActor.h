/* Start Header -------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: <BlackHole.h>
Purpose: <Player's ability create bomb>
Language: <c++>
Platform: <Unreal Engine>
Project: <GAM300>
Author: <kevin kim: hyunseok.kim>
End Header --------------------------------------------------------*/
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSBombActor.generated.h"


class UStaticMeshComponent;
class UParticleSystem;

UCLASS()
class FPSGAME_API AFPSBombActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPSBombActor();

protected:
	UPROPERTY(EditDefaultsOnly, Category= "BombActor")
	float ExplodeDelay;

	UPROPERTY(EditDefaultsOnly, Category = "BombActor")
	UParticleSystem* ExplosionTemplate;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* MeshComp;

	
	UMaterialInstanceDynamic* MaterialInst;

	FLinearColor CurrentColor;
	FLinearColor TargetColor;
	
	
	UFUNCTION()
	void Explode();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
