/* Start Header -------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: <BlackHole.h>
Purpose: <Player's ability create blackhole>
Language: <c++>
Platform: <Unreal Engine>
Project: <GAM300>
Author: <kevin kim: hyunseok.kim>
End Header --------------------------------------------------------*/
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlackHole.generated.h"

class USphereComponent;
class UStaticMeshComponent;

UCLASS()
class FPSGAME_API ABlackHole : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABlackHole();

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UStaticMeshComponent* MeshComp;

	//Inner sphere destroys the overlapping components
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USphereComponent* InnerSphereComponent;

	//Outer sphere pulls components, which are physically simulating, towards the center
	UPROPERTY(VisibleAnywhere, Category = "Components")
		USphereComponent* OuterSphereComponent;

	UPROPERTY(EditAnywhere, Category = "Variable")
		float OuterRadialForce;
	UPROPERTY(EditAnywhere, Category = "Variable")
		float InnerRadialForce;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
